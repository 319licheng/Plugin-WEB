package org.extreme.web.core.aop;

import org.extreme.web.core.action.IAction;


public class ActionInvoker {
	
	private IAction action;
	
	private InterceptorStack stack;
	
	public void invoke(){
		if(stack.isLatest()) {
			action.process();
			return ;
		}
		Interceptor in = stack.getNext();
		in.intercept(this);
	}

	public IAction getAction() {
		return action;
	}

	public void setAction(IAction action) {
		this.action = action;
	}

	public InterceptorStack getStack() {
		return stack;
	}

	public void setStack(InterceptorStack stack) {
		this.stack = stack;
	}
	
	
}
