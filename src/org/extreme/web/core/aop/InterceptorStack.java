package org.extreme.web.core.aop;

import java.util.ArrayList;
import java.util.List;

import org.extreme.web.core.env.Env;

public class InterceptorStack {
	
	private List<Interceptor> ins;
	
	public Interceptor getNext(){
		if(isLatest()) return null;
		Env.get().setInterceptorIndex(Env.get().getInterceptorIndex()+1);
		return ins.get(Env.get().getInterceptorIndex()-1);
	}
	
	public boolean isLatest(){
		if(ins==null) return true;
		return Env.get().getInterceptorIndex()==(ins.size());
	}

	public List<Interceptor> getIns() {
		return ins;
	}

	public void setIns(List<Interceptor> ins) {
		this.ins = ins;
	}
	
	public void add(Interceptor in){
		if(ins==null) ins=new ArrayList<Interceptor>();
		ins.add(in);
	}
	
	public void add(List<Interceptor> ins){
		if(ins==null) ins=new ArrayList<Interceptor>();
		ins.addAll(ins);
	}
}
