package org.extreme.web.core.aop;

public interface Interceptor {
	public void intercept(ActionInvoker ai);
}