package org.extreme.web.core.cfg;

public interface Methods {
    public static final String POST = "POST";
    public static final String GET = "GET";
    public static final String HEAD = "HEAD";
    public static final String OPTIONS = "OPTIONS";
    public static final String PUT = "PUT";
    public static final String DELETE = "DELETE";
    public static final String TRACE = "TRACE";
    public static final String[] ALL_METHODS = {POST,GET,HEAD,OPTIONS,PUT,DELETE,TRACE};
}
