package org.extreme.web.core.cfg;

import org.extreme.web.core.ctrl.ICtrl;
import org.extreme.web.core.resolver.IResolver;
import org.extreme.web.core.resolver.StrResolver;
import org.extreme.web.core.route.IRoutes;
import org.extreme.web.core.route.ListStrRoutes;
import org.extreme.web.core.view.IRender;
import org.extreme.web.core.view.JspRender;

/**
 * web插件的配置类
 * @author Administrator
 *
 */
public abstract class WebCfg {
	
	/**
	 * 入口方法
	 */
	public abstract void registerRoutes();
	


	public void registerPlugins() {
	}

	public void configView() {
	}

	public void configResolver() {
	}
	
	public void setDefaultRender(IRender render){
		WebGlobal.setDefaultRender(render);
	}
	
	protected void register(ICtrl ctrl,String path){
		WebGlobal.getRoutes().bind(ctrl, path);
	}
	
	protected void settings(){
		
	}
	
	public void init(){
		
		if(WebGlobal.getDefaultRender()==null) WebGlobal.setDefaultRender(new JspRender());
		if(WebGlobal.getRoutes()==null) WebGlobal.setRoutes(new ListStrRoutes());
		if(WebGlobal.getResolver()==null) WebGlobal.setResolver(new StrResolver());
		WebGlobal.getResolver().setRoutes(WebGlobal.getRoutes());
		settings();
		
		registerRoutes();
		
		registerPlugins();
		
        configView();
		
        configResolver();
	}

	public IRender getDefaultRender() {
		return WebGlobal.getDefaultRender();
	}

	public IRoutes getRoutes() {
		return WebGlobal.getRoutes();
	}

	public IResolver getResolver() {
		return WebGlobal.getResolver();
	}

	public void setResolver(IResolver resolver) {
		WebGlobal.setResolver(resolver);
	}
	
}
