package org.extreme.web.core.cfg;

import org.extreme.core.plugin.Plugins;
import org.extreme.web.core.resolver.IResolver;
import org.extreme.web.core.resolver.StrResolver;
import org.extreme.web.core.route.IRoutes;
import org.extreme.web.core.route.ListStrRoutes;
import org.extreme.web.core.view.BeetlRender;
import org.extreme.web.core.view.IRender;

public class WebGlobal{
		
	public static final String CHARSET = "utf-8";
	
	public static final String INDEX_METHOD_NAME = "index";
	
    private static IRender defaultRender = new BeetlRender();
    private static IRoutes routes = new ListStrRoutes();
    private static IResolver resolver = new StrResolver();
    private static Plugins plugins = new Plugins();
    
	public static IRender getDefaultRender() {
		return defaultRender;
	}

	public static void setDefaultRender(IRender defaultRender) {
		WebGlobal.defaultRender = defaultRender;
	}

	public static IRoutes getRoutes() {
		return routes;
	}

	public static void setRoutes(IRoutes routes) {
		WebGlobal.routes = routes;
	}

	public static IResolver getResolver() {
		return resolver;
	}

	public static void setResolver(IResolver resolver) {
		WebGlobal.resolver = resolver;
	}

	public static Plugins getPlugins() {
		return plugins;
	}

	public static void setPlugins(Plugins plugins) {
		WebGlobal.plugins = plugins;
	}
    
    
}
