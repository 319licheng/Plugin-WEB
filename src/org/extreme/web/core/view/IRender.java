package org.extreme.web.core.view;

import javax.servlet.http.HttpServletResponse;


public interface IRender {
	public void render(String context);
	public void render404();
	public void renderStatus(HttpServletResponse resp,String contentType,int status);
	public void renderStr(String str);
}
