package org.extreme.web.core.view;

import java.io.IOException;

import org.beetl.core.Configuration;
import org.beetl.core.GroupTemplate;
import org.beetl.core.ResourceLoader;
import org.beetl.core.exception.BeetlException;
import org.beetl.core.resource.WebAppResourceLoader;
import org.beetl.ext.web.WebRender;
import org.extreme.core.exception.PluginsException;
import org.extreme.web.core.env.Env;

public class BeetlRender extends AbstractRender {
	
	private Configuration cfg;
	
	private GroupTemplate gt;
	
	private ResourceLoader loader;
	
	private WebRender render;
	
	public BeetlRender() {
		try{
			cfg = Configuration.defaultConfiguration();
			loader = new WebAppResourceLoader();
			gt = new GroupTemplate(loader, cfg);
			render = new WebRender(gt);
		}catch(IOException e){
			throw new RuntimeException("load beetl template failed!");
		}
	}
	
	@Override
	public void render(String context) {
		Env.res().setContentType(AbstractRender.contentType);
		render.render(context, Env.req(), Env.res());
	}

	/**处理客户端抛出的IO异常
	 * @param ex
	 */
	protected void handleClientError(IOException ex)
	{
		throw new PluginsException("beetl render error");
	}

	/**处理客户端抛出的IO异常
	 * @param ex
	 */
	protected void handleBeetlException(BeetlException ex)
	{
		throw ex;
	}
}



