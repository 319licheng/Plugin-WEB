package org.extreme.web.core.view;

import javax.servlet.RequestDispatcher;

import org.extreme.web.core.action.ActionException;
import org.extreme.web.core.env.Env;


public class JspRender extends AbstractRender {

	@Override
	public void render(String context) {
		try {
			RequestDispatcher dispatcher =
			Env.req()
				.getRequestDispatcher(context);
			dispatcher.forward(
							Env.req(),
							Env.res()
					);
		} catch (Exception e) {
			e.printStackTrace();
			throw new ActionException("render jsp err.");
		}
	}

}
