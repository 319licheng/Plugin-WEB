package org.extreme.web.core.view;

import java.io.PrintWriter;

import javax.servlet.http.HttpServletResponse;

import org.extreme.web.core.action.ActionException;
import org.extreme.web.core.cfg.WebGlobal;
import org.extreme.web.core.env.Env;

public abstract class AbstractRender implements IRender{
	
	public static final String contentType = "text/html; charset="+WebGlobal.CHARSET;
	
	public void render(HttpServletResponse resp,String s,String contentType,String encoding,int status){
		PrintWriter pw = null;
		resp.setStatus(status);
		try {
			pw = resp.getWriter();
			resp.setContentType(contentType);
			resp.setCharacterEncoding(encoding);
			pw.write(s);
			pw.flush();
		} catch (Exception e) {
			e.printStackTrace();
			throw new ActionException("output text err.");
		}finally{
			if(pw!=null) pw.close();
		}
	}
	
	public void renderStatus(HttpServletResponse resp,String contentType,int status){
		resp.setContentType(contentType);
		resp.setStatus(status);
	}
	
	public void render404(){
		render(Env.res(),"<h1>404</h1>",contentType,WebGlobal.CHARSET,404);
	}
	
	public void renderStr(String str){
		render(Env.res(),str,contentType,WebGlobal.CHARSET,200);
	}
}
