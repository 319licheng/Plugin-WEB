package org.extreme.web.core.ctrl;

import java.util.List;

import org.extreme.web.core.action.IAction;

/**
 * ctrl控制器是一个action的集合。
 * @author Administrator
 *
 */
public interface ICtrl {
	public List<IAction> toActions();
	public String getPath();
	public void setPath(String path);
}
