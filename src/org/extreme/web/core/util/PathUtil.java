package org.extreme.web.core.util;

import java.io.File;


public class PathUtil {

	private static String webroot;
	
	private static String detectWebRootPath() {
		try {
			String path = PathUtil.class.getResource("/").toURI().getPath();
			return new File(path).getParentFile().getParentFile().getCanonicalPath();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	public static String getWebRootPath() {
		if(webroot!=null) return webroot;
		return detectWebRootPath();
	}
}
