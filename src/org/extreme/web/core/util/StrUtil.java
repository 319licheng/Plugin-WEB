package org.extreme.web.core.util;

public class StrUtil {
	
	public static boolean contains(String[] arr,String str){
		if(str==null) return false;
		for(String s : arr){
			if(str.equals(s)) return true;
		}
		return false;
	}
}
