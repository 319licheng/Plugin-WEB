package org.extreme.web.core.action;

import org.extreme.web.core.aop.ActionInvoker;


public abstract class AbstractAction implements IAction {
	
	/**
	 * 召唤者 卡尔
	 */
	protected ActionInvoker invoker;
	
	@Override
	public void process(){
		
	}

	public ActionInvoker getInvoker() {
		return invoker;
	}

	public void setInvoker(ActionInvoker invoker) {
		this.invoker = invoker;
	}
	
	
	
}
