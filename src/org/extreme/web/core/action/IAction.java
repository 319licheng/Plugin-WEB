package org.extreme.web.core.action;

import org.extreme.web.core.aop.ActionInvoker;
import org.extreme.web.core.ctrl.ICtrl;


/**
 * 最小的请求处理单元
 * @author lichee
 *
 */
public interface IAction {
    
	public void process();
	
	public String getActionKey();
	
	/**
	 * 获取action支持的方法
	 * @return
	 */
	public String[] getMethods();
	
	public ActionInvoker getInvoker();
	
	public ICtrl getICtrl();
}
