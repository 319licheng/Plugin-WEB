package org.extreme.web.core.action;

import java.lang.reflect.Method;

import org.extreme.web.core.ctrl.ICtrl;


public class CtrlAction extends AbstractAction {
	
	private String[] methods;
	
	private Method methodObj;
	
	private ICtrl ctrl;//来源控制器
	
	private String actionKey;//路径标志
	
	public CtrlAction(ICtrl ctrl,Method methodObj,String[] methods,String actionKey){
		this.methods = methods;
		this.methodObj = methodObj;
		this.ctrl = ctrl;
		this.actionKey = actionKey;
	}
	
	@Override
	public void process(){
		try {
			methodObj.invoke(ctrl);
		} catch (Exception e) {
			e.printStackTrace();
			throw new ActionException("Action invoke error! ");
		}
	}
	
	@Override
	public String[] getMethods() {
		return methods;
	}
	
	@Override
	public String getActionKey() {
		return actionKey;
	}

	@Override
	public ICtrl getICtrl() {
		return ctrl;
	}
}
