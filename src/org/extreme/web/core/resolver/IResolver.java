package org.extreme.web.core.resolver;

import org.extreme.web.core.action.IAction;
import org.extreme.web.core.route.IRoutes;

/**
 * resolver的作用是将请求解析到Action
 * @author Administrator
 *
 */
public interface IResolver {
	
	public IAction resolvePath();

	public IRoutes getIRoutes();

	public void setRoutes(IRoutes routes);
}
