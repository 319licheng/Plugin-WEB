package org.extreme.web.core.resolver;

import org.extreme.web.core.action.IAction;
import org.extreme.web.core.env.Env;
import org.extreme.web.core.route.IRoutes;

public class StrResolver extends AbstractResolver{ 
	
	private IRoutes routes;
	
	@Override
	protected IAction getAction() {
		IAction action = routes.get(Env.getActionKey(), Env.req().getMethod());
		return action;
	}

	@Override
	public IRoutes getIRoutes() {
		return routes;
	}

	@Override
	public void setRoutes(IRoutes routes) {
		this.routes = routes;
	}
	
}
