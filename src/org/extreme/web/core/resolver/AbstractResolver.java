package org.extreme.web.core.resolver;

import org.extreme.web.core.action.IAction;

public abstract class AbstractResolver implements IResolver {

	@Override
	public IAction resolvePath() {
		IAction action = getAction();
		return action;
	}
	
	/**
	 * 请复写此方法
	 * @param uri
	 * @return
	 */
	protected abstract IAction getAction();
}
