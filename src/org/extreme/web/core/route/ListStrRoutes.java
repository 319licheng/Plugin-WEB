package org.extreme.web.core.route;

import java.util.ArrayList;
import java.util.List;

import org.extreme.web.core.action.IAction;
import org.extreme.web.core.ctrl.ICtrl;
import org.extreme.web.core.util.StrUtil;

public class ListStrRoutes extends AbstractRoutes{
	
	private List<IAction> routesList;
	private List<ICtrl> ctrlList;
	
	public ListStrRoutes(){
		routesList = new ArrayList<IAction>(50);
		ctrlList = new ArrayList<ICtrl>(15);
	}
	
	@Override
	public void add(IAction action) {
		routesList.add(action);
	}

	@Override
	public void remove(IAction action) {
		routesList.remove(action);
	}

	@Override
	public IAction get(String actionKey,String method) {
		if(actionKey==null || method==null) return null;
		for(IAction action : routesList){
			if(actionKey.equals(action.getActionKey()) 
					&& StrUtil.contains(action.getMethods(), method)){
				return action;
			}
		}
		return null;
	}

	@Override
	public IRoutes bind(ICtrl ctrl, String path) {
		ctrl.setPath(path);
		for(IAction action : ctrl.toActions()){
			add(action);
		}
		if(!ctrlList.contains(ctrl)){
			ctrlList.add(ctrl);
		}
		return this;
	}

	@Override
	public List<ICtrl> getCtrls() {
		return ctrlList;
	}

}
