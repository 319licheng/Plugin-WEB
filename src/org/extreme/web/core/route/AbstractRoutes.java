package org.extreme.web.core.route;

import org.extreme.web.core.action.IAction;
import org.extreme.web.core.ctrl.ICtrl;

public abstract class AbstractRoutes implements IRoutes {

	@Override
	public void add(ICtrl ctrl) {
		for(IAction action : ctrl.toActions()){
			add(action);
		}
	}

}
