package org.extreme.web.core.route;

import java.util.List;

import org.extreme.web.core.action.IAction;
import org.extreme.web.core.ctrl.ICtrl;

/**
 * 路由表
 * @author lich
 *
 */
public interface IRoutes {
	
	public void add(IAction action);
	
	public void add(ICtrl ctrl);
	
	public void remove(IAction action);
	
	public IAction get(String actionKey,String method);
	
	public IRoutes bind(ICtrl ctrl,String paht);
	
	public List<ICtrl> getCtrls();

}
