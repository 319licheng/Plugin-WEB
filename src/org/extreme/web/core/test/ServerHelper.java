package org.extreme.web.core.test;

import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.webapp.WebAppContext;
import org.extreme.core.servlet.PluginServlet;
import org.extreme.web.core.servlet.MainServlet;

public class ServerHelper {

    public static void server(int port, String mapping, String configClassName) throws Exception {
        server(port, handler(mapping, configClassName));
    }

    public static void server(int port, Handler handler) throws Exception {
        Server server = new Server(port);
        server.setHandler(handler);
        server.start();
        server.join();
    }

    public static ServletContextHandler handler(String mapping, String configClass) {
        ServletContextHandler handler = new ServletContextHandler(ServletContextHandler.SESSIONS);
        handler.setContextPath("/");
        handler.setResourceBase("/WebContent");
        ServletHolder holder = new ServletHolder(new PluginServlet());
        holder.setInitParameter("configClass", configClass);
        handler.addServlet(holder,mapping);
        return handler;
    }

    public static WebAppContext webapp() {
        WebAppContext root = new WebAppContext();
        root.setContextPath("/");
        root.setDescriptor("WebContent/WEB-INF/web.xml");
        root.setResourceBase("WebContent/");
        root.setParentLoaderPriority(true);
        return root;
    }

}