package org.extreme.web.plugins.du;

public class DuException extends RuntimeException {


    public DuException() {
        super();
    }

    public DuException(String msg) {
        super(msg);
    }

    public DuException(Throwable cause) {
        super(cause);
    }

    private static final long serialVersionUID = 1L;	
}
