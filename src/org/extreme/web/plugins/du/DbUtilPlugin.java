package org.extreme.web.plugins.du;

import javax.servlet.ServletConfig;
import javax.sql.DataSource;

import org.extreme.core.plugin.AbstractPlugin;

public class DbUtilPlugin extends AbstractPlugin {
	
	private DataSource ds;
	
	public DbUtilPlugin(DataSource ds) {
		this.ds = ds;
	}

	@Override
	public void init(ServletConfig config) {
		// DO NOTHING
	}

	@Override
	public void destroy() {
		// DO NOTHING
	}
}
