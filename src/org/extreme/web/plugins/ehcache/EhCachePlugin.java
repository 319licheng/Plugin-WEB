package org.extreme.web.plugins.ehcache;

import java.io.InputStream;
import java.net.URL;

import javax.servlet.ServletConfig;

import net.sf.ehcache.CacheManager;
import net.sf.ehcache.config.Configuration;

import org.extreme.core.plugin.AbstractPlugin;

public class EhCachePlugin extends AbstractPlugin {
	
	private static CacheManager cacheManager;
	private String configurationFileName;
	private URL configurationFileURL;
	private InputStream inputStream;
	private Configuration configuration;
	
	public EhCachePlugin() {
		
	}
	
	public EhCachePlugin(CacheManager cacheManager) {
		EhCachePlugin.cacheManager = cacheManager;
	}
	
	public EhCachePlugin(String configurationFileName) {
		this.configurationFileName = configurationFileName; 
	}
	
	public EhCachePlugin(URL configurationFileURL) {
		this.configurationFileURL = configurationFileURL;
	}
	
	public EhCachePlugin(InputStream inputStream) {
		this.inputStream = inputStream;
	}
	
	public EhCachePlugin(Configuration configuration) {
		this.configuration = configuration;
	}

	@Override
	public void init(ServletConfig config) {
		createCacheManager();
		EhUtil.init(cacheManager);
	}

	@Override
	public void destroy() {
	}
	
	
	private void createCacheManager() {
		if (cacheManager != null)
			return ;
		
		if (configurationFileName != null) {
			cacheManager = CacheManager.create(configurationFileName);
			return ;
		}
		
		if (configurationFileURL != null) {
			cacheManager = CacheManager.create(configurationFileURL);
			return ;
		}
		
		if (inputStream != null) {
			cacheManager = CacheManager.create(inputStream);
			return ;
		}
		
		if (configuration != null) {
			cacheManager = CacheManager.create(configuration);
			return ;
		}
		
		cacheManager = CacheManager.create();
	}
}