package org.extreme.core.exception;

public class PluginsException extends RuntimeException {

    public PluginsException() {
        super();
    }

    public PluginsException(String msg) {
        super(msg);
    }

    public PluginsException(Throwable cause) {
        super(cause);
    }

    private static final long serialVersionUID = 1L;

}
