package org.extreme.core.servlet;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.extreme.core.plugin.Plugins;

public class PluginServlet extends HttpServlet {
	
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
    
	@Override
	public void service(HttpServletRequest req, HttpServletResponse resp)
	        throws ServletException, IOException {
        try{
        	Plugins.me.before(req, resp);
        	Plugins.me.service(req, resp);
        	Plugins.me.after(req, resp);
        }catch(Exception e){
        	e.printStackTrace();
        	resp.setStatus(500);
        }
	}
	


    @Override
    public void init(ServletConfig config) throws ServletException {
    	Plugins.me.init(config);
    }
    
    @Override
    public void destroy(){
    	Plugins.me.destroy();
    }
}
