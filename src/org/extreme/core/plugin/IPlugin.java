package org.extreme.core.plugin;

import javax.servlet.ServletConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public interface IPlugin {
	
	public void init(ServletConfig config);
	
	public void before(HttpServletRequest req, HttpServletResponse resp);
	
	public void service(HttpServletRequest req, HttpServletResponse resp);
	
	public void after(HttpServletRequest req, HttpServletResponse resp);

	public void destroy();
	
}
