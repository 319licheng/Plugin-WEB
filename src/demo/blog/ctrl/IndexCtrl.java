package demo.blog.ctrl;

import org.extreme.web.core.action.Can;
import org.extreme.web.core.aop.Aop;
import org.extreme.web.core.ctrl.AbstractCtrl;
import org.springframework.beans.factory.annotation.Autowired;

import demo.blog.service.UserService;

@Aop(CtrlInterceptor.class)
public class IndexCtrl extends AbstractCtrl {
	
	@Autowired
	private UserService userService;
	
	@Can({"GET"})
	@Aop(ActionInterceptor.class)
	public void test(){
		//renderStr("jbd");
		System.out.println("action invoke...");
		userService.testB();
		setAttr("ff","ff var");
		setAttr("aa","aaa var");
		render("/test.btl");
	}
	
}
