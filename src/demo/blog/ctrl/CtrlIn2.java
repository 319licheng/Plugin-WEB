package demo.blog.ctrl;

import org.extreme.web.core.aop.ActionInvoker;
import org.extreme.web.core.aop.Interceptor;

public class CtrlIn2 implements Interceptor {

	@Override
	public void intercept(ActionInvoker ai) {
		System.out.println("aop:before ctrl 2");
		ai.invoke();
		System.out.println("aop:after ctrl 2");
	}

}
