package demo.blog.ctrl;

import org.extreme.web.core.aop.ActionInvoker;
import org.extreme.web.core.aop.Interceptor;

public class ActionInterceptor implements Interceptor {

	@Override
	public void intercept(ActionInvoker ai) {
		System.out.println("aop:before action 1");
		ai.invoke();
		System.out.println("aop:after action 1");
	}

}
