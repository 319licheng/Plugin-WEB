package demo.blog;

import org.extreme.web.core.cfg.WebCfg;

import demo.blog.ctrl.IndexCtrl;

public class WebConfig extends WebCfg {

	@Override
	public void registerRoutes() {
		register(new IndexCtrl(), "app");
	}
}
