package demo.blog;

import org.extreme.core.cfg.AbstractConfig;
import org.extreme.core.plugin.Plugins;
import org.extreme.web.core.support.WebPlugin;
import org.extreme.web.core.test.ServerHelper;
import org.extreme.web.plugins.spring.SpringPlugin;

public class AppConfig extends AbstractConfig {

	@Override
	public void ConfigPlugins(Plugins p) {
		p.add(new WebPlugin(WebConfig.class))
			.add(new SpringPlugin());
	}
	
	public static void main(String[] args) throws Exception{
		ServerHelper.server(3000, "/", "demo.blog.AppConfig");
	}
}
